#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int raw_sequential_lcs(char* string1, char* string2, int len_s1, int len_s2) {
	int rows = strlen(string1) + 1;
	int cols = strlen(string2) + 1;

	int lcs[rows][cols];
	//int traceback[rows][cols];

	for (int col = 0; col < cols; col++)
		lcs[0][col] = 0;
	for (int row = 1; row < rows; row++)
		lcs[row][0] = 0;

	for (int row = 1; row < rows; row++) {
		for (int col = 1; col < cols; col++) {
			if (string1[row - 1] == string2[col - 1]) {
				lcs[row][col] = 1 + lcs[row - 1][col - 1];
				//traceback[row][col] = 3;
			}
			else if (lcs[row - 1][col] >= lcs[row][col - 1]) {
				lcs[row][col] = lcs[row - 1][col];
				//traceback[row][col] = 1;
			}
			else {
				lcs[row][col] = lcs[row][col - 1];
				//traceback[row][col] = 2;
			}
		}
	}


	/*for (int row = 0; row < rows; row++) {
		for (int col = 0; col < cols; col++) {
			printf("%d ", traceback[row][col]);
		}
		printf("\n");
	}*/


	return lcs[rows - 1][cols - 1];
}

int main() {
	printf("LCS: %d\n", raw_sequential_lcs("abcd", "bc"));
}
