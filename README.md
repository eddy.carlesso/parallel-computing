# Parallel Computing

This project implements the Longest SubSequence Problem - LCS - in two files:
- sequential_lcs: contains the sequential implementation of the LCS Problem

- parallel_lcs: contains the parallel implementation of the LCS PRoblem


The file raw_sequential_lcs is a first, non-optimized version of the sequential algorithm.
