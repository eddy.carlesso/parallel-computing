#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>

int parallel_lcs(char* string1, char *string2, int rank, int size) {
    int rows = strlen(string1) + 1;
    int cols = strlen(string2) + 1;

    int lcs[3][cols];
    MPI_Status status;
	
    int col;

    for (int count_row = 1; count_row < rows + cols; count_row++) {
        int current_row = count_row % 3;
        int previous_row = (count_row-1) % 3;
		
        int start_column = 0;
		
		if (count_row > rows){
			start_column = count_row - rows;
		}
		int end_column = count_row;
		if (cols - start_column < end_column){
			end_column = cols - start_column;
		}

		//a processor needs to process only its assigned elements
		//if processors are more than the elements to be processed just assign by the index of the element
        int begin, finish;
        if (end_column <= size) {
            begin = rank;
			finish = rank;
			//if there are more	 processor than elements, forbid to enter the for loop
			if (end_column - 1 < finish){
				finish = end_column - 1;
			}
        } else {
			//there are more lements than processors, we need to split the work
            float n_elements = (float)end_column / size;
            begin = round(n_elements * rank);
            finish = round(n_elements * (rank + 1)) - 1;
        }

        for (int count_col = begin; count_col <= finish; count_col++) {
			int row = count_row;
			if (rows < row){
				//I'm looking at the last row
				row = rows;
			}
            row = row - count_col - 1;
            col = start_column + count_col;
			
            if (row == 0 || col == 0) {
				//base case
                lcs[current_row][col] = 0;
            }
            else if (string1[row - 1] == string2[col - 1]) {
                lcs[current_row][col] = 1 + lcs[(count_row-2) % 3][col-1];
            }
            else if (lcs[previous_row][col - 1] >= lcs[previous_row][col]){
                lcs[current_row][col] = lcs[previous_row][col - 1];
            } else {
                lcs[current_row][col] = lcs[previous_row][col];
			}

			//send data to others
			//the first has nothing to send
            if (rank > 0 && count_col == begin) {
                MPI_Send(&lcs[current_row][col], 1, MPI_INT, rank - 1, 1, MPI_COMM_WORLD);
            }
			//the last doesn't need to send anything
            if (rank < size - 1 && count_col == finish) {
                MPI_Send(&lcs[current_row][col], 1, MPI_INT, rank + 1, 1, MPI_COMM_WORLD);
            }
        }

		//receive data from others
        int prev_index = begin - 1;
        if (begin > 0 && begin - 1 < end_column) {
            col = start_column + begin - 1;
            MPI_Recv(&lcs[current_row][col], 1, MPI_INT, rank - 1, 1, MPI_COMM_WORLD, &status);
        }

        if (finish + 1 >= 0 && finish < end_column - 1) {
            col = start_column + finish + 1;
            MPI_Recv(&lcs[current_row][col], 1, MPI_INT, rank + 1, 1, MPI_COMM_WORLD, &status);
        }
    }

    int result = (rows + cols - 1) % 3;
	return lcs[result][cols - 1];
}


int main(int argc, char** argv) {

	int taskId = 0;
	int rank, size;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

    char *string1, *string2;


    double start, finish;
    if (rank == 0){
        start = MPI_Wtime();
	}

    int lcs = parallel_lcs("abcd", "ab", rank, size);

    if (rank == 0) {
        finish = MPI_Wtime();
        printf("TIME: %f\n", finish - start);
        printf("LCS: %d \n", lcs);
    }

    MPI_Finalize();
	return 0;
}

