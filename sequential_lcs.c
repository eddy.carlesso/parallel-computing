#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int improved_sequential_lcs(char* string1, char* string2) {
	int rows = strlen(string1) + 1;
	int cols = strlen(string2) + 1;

	int lcs[3][cols];
	//int traceback[rows][cols];

	for (int count_row = 1; count_row < rows + cols; count_row++) { 
	//rows + cols - 1 = number of diagonals
		int current_row = count_row % 3;
		int previous_row = (count_row - 1) % 3;
		int start_column = 0;
		if (rows < count_row){
			//I'm looking at diagonals not starting from the column 0
			start_column = count_row - rows;
		}
		int end_cloumn = count_row;
		if (cols - start_column < end_cloumn){
			//I'm looking at diagonals not strating from the column 0
			end_cloumn = cols - start_column;
		}
		for (int count_col = 0; count_col < end_cloumn; count_col++) {
			int row = count_row;
			if (count_row - rows > 0){
				//I'm looking at the last row
				row = rows;
			}
			//get one position up for every column movement
			row = row - count_col - 1;
			int col = start_column + count_col;
			if (row == 0 || col == 0){
				//base case 
				lcs[current_row][col] = 0;
			} else if (string1[row - 1] == string2[col - 1]) {
				lcs[current_row][col] = 1 + lcs[(count_row - 2) % 3][col - 1];
				//traceback[row][col] = 3;
			} else if (lcs[previous_row][col - 1] >= lcs[previous_row][col]) {
				lcs[current_row][col] = lcs[previous_row][col - 1];
				//traceback[row][col] = 1;
			} else {
				lcs[current_row][col] = lcs[previous_row][col];
				//traceback[row][col] = 2;
			}
		}
	}

/*
	for (int row = 0; row < rows; row++) {
		for (int col = 0; col < cols; col++) {
			printf("%d ", traceback[row][col]);
		}
		printf("\n");
	}
*/

	int result = (rows + cols - 1) % 3;
	return lcs[result][cols - 1];
}

int main() {
	printf("LCS: %d\n", sequential_lcs("abcd", "bc"));
}
